/**
 * Date and time handling
 * @module
 */

/**
 * Month index [0..11]
 */
export type MonthIdx = number;

/**
 * Day index [0..6]
 * 0 is Sunday, 6 is Saturday
 */
export type DayIdx = number;

/**
 * Year
 */
export type Year = number;

/**
 * Season
 */
export type Season = 'winter' | 'spring' | 'summer' | 'autumn';

/**
 * Millisecondy per day
 */
export const millisecondsPerDay = 1000 * // milliseconds per second
  60 * // seconds per minute
  60 * // minutes per hour
  24; // hours per day

/**
 * Days in year
 */
export const daysInYear = 365;

/**
 * Days in leap year
 */
export const daysInLeapYear = 366;

/**
 * Get days in month
 * @param year Year
 * @param month Month to get number of days for [0..11]
 * @returns Number of days in month
 * @see https://stackoverflow.com/questions/1184334/get-number-days-in-a-specified-month-using-javascript
 */
export function getDaysInMonth(year: number, month: MonthIdx): number {
  // due to the arithmetic of JavaScript dates this equates to:
  // the last day before the the first day in the next month
  return new Date(year, month + 1, 0).getDate();
}

/**
 * Get day of year
 * @param date Date to get day of year for
 * @returns Day of year
 * @see https://stackoverflow.com/questions/8619879/javascript-calculate-the-day-of-the-year-1-366
 */
export function getDayOfYear(date: Date): number {
  const startOfYear = new Date(date.getFullYear(), 0, 0);
  const diff = (date.getTime() - startOfYear.getTime()) +
    ((startOfYear.getTimezoneOffset() - date.getTimezoneOffset()) * 60 * 1000);
  return Math.floor(diff / millisecondsPerDay);
}

/**
 * Get number of days in year
 * @param year Year to get number of days for
 * @returns Number of days in year
 */
export function getDaysInYear(year: Year): number {
  return isLeap(year) ? daysInLeapYear : daysInYear;
}

/**
 * Get a list of specific days in a month
 * @param year Year to get days in
 * @param monthIdx Index of month to get days in [0..11]
 * @param dayIdx Index of day of week to get days for [0..6]
 * @returns An array of days
 */
export function getSpecificDaysInMonth(
  year: number,
  monthIdx: MonthIdx,
  dayIdx: DayIdx,
): Date[] {
  // initialize result
  const resultDays: Date[] = [];

  // get number of days in month
  const numberOfDaysInMonth = getDaysInMonth(year, monthIdx);

  // get iterator day, first day of month
  const iteratorDay = new Date(year, monthIdx, 1, 0, 0, 0, 0);

  // iterate over days in month
  for (let i = 1; i <= numberOfDaysInMonth; i++) {
    // set date on iterator day
    iteratorDay.setDate(i);

    // check if day of week matches the specified index
    if (iteratorDay.getDay() === dayIdx) {
      // copy iterator day and add it to result
      resultDays.push(new Date(iteratorDay));
    }
  }

  return resultDays;
}

/**
 * Format a date
 * @param date Date to format
 * @param language Language to format date in
 * @param format Format to format with
 * @returns Formatted date
 */
export function formatDate(
  date: Date,
  language: Intl.LocalesArgument,
  format: Intl.DateTimeFormatOptions,
): string {
  const formatter = new Intl.DateTimeFormat(language, format);
  return formatter.format(date);
}

/**
 * Get season of date
 * @param date Date to get season from
 * @returns Season
 */
export function getSeason(date: Date): Season {
  const monthIdx = date.getMonth();
  switch (monthIdx) {
    case 2:
    case 3:
    case 4:
      return 'spring';
    case 5:
    case 6:
    case 7:
      return 'summer';
    case 8:
    case 9:
    case 10:
      return 'autumn';
    default:
      return 'winter';
  }
}

/**
 * Determine whether or not a year is a leap year
 * @param year Year to check
 * @returns Whether or not provided year is a leap year
 * @see https://stackoverflow.com/questions/8175521/javascript-to-find-leap-year
 */
export function isLeap(year: number): boolean {
  return new Date(year, 1, 29).getMonth() === 1;
}
