/**
 * Common types, functions and variables
 *
 * @module
 */

import { JSDOM } from 'jsdom';
import { existsSync } from 'node:fs';
import { mkdir, readFile, writeFile } from 'node:fs/promises';
import { dirname } from 'node:path';
import type { Flags } from './cli.ts';

import { createLogger, format, type Logger, transports } from 'winston';

/**
 * Logger
 */
export const logger: Logger = createLogger({
  level: 'debug',
  transports: [
    new (transports.Console)({
      format: format.combine(
        format.colorize({
          all: true,
        }),
        format.printf(
          (info) => `[${info.level}] ${info.message}`,
        ),
      ),
    }),
  ],
});

/**
 * Slugify a filename
 * @param fileName File name to slugify
 * @returns A sluggified filename
 */
export function slugify(fileName: string): string {
  return fileName.toString()
    .replace(/{/g, '__')
    .replace(/}/g, '_')
    .replace(/"/g, '')
    .replace(/:/g, '_')
    .replace(/,/g, '-');
}

/**
 * Get source from local or remote
 *
 * Option --ignoreCache: remote source will be used.
 * Option --cacheRemoteResources: remote source will be cached as local source file.
 * @param localSourceFile Local source file
 * @param remoteSource Remote source
 * @returns Gotten source
 */
export async function getSource(
  localSourceFile: string,
  remoteSource: string,
  flags: Flags,
): Promise<string> {
  let source = '';

  if (
    existsSync(localSourceFile) &&
    !flags.ignoreCache
  ) {
    logger.info(`Using local source file ${localSourceFile}.`);
    source = (await readFile(localSourceFile)).toString();
  } else {
    logger.info(`Using remote source ${remoteSource}.`);

    const response = await fetch(remoteSource);
    source = await response.text();

    if (flags.cacheRemoteSources) {
      logger.info(`Saving remote source as ${localSourceFile}.`);
      await mkdir(dirname(localSourceFile), {
        recursive: true,
      });
      await writeFile(localSourceFile, source);
    }
  }

  return source;
}

// instantiate once on module load
const { document } = (new JSDOM(`...`)).window;

/**
 * Strip tags
 * @param html HTML to strip tags fro
 * @returns String without tags
 * @see https://stackoverflow.com/questions/822452/strip-html-tags-from-text-using-plain-javascript
 */
export function stripTags(html: string): string {
  const element = document.createElement('div') as HTMLDivElement;
  element.innerHTML = html;
  return element.textContent || element.innerText || '';
}
