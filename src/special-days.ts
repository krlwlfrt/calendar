import { getDayOfYear } from './datetime.ts';

/**
 * A special day
 */
export class SpecialDay {
  /**
   * Create a new commemorative day
   * @param title Title of the commemorative day
   * @param date Date of the commemorative day
   * @param sinceYear Year, since when the special day exists
   */
  constructor(
    public title: string,
    public date: Date,
    public sinceYear?: number,
  ) {}

  /**
   * Convert most relevant information of day to string
   * @returns Stringified information of day
   */
  public toString(): string {
    return `${getDayOfYear(this.date)} ${this.title}`;
  }
}

/**
 * A commemorative day
 */
export class CommemorativeDay extends SpecialDay {
  /**
   * Additional text that further describes the day
   */
  description?: string;

  /**
   * Whether or not this day is a holiday
   */
  holiday = false;

  /**
   * Whether or not the day moves from year to year
   */
  moving = false;

  /**
   * Country, religion, region, ... where the day is valid
   */
  scope?: string;

  /**
   * Type of the day
   */
  type?: string;

  /**
   * Create a new commemorative day
   * @param title Title
   * @param date Date
   * @param sinceYear Since year
   * @param additionalData Additional data
   */
  constructor(
    title: string,
    date: Date,
    sinceYear?: number,
    additionalData?: Partial<
      Omit<CommemorativeDay, 'title' | 'date' | 'sinceYear'>
    >,
  ) {
    super(title, date, sinceYear);

    this.description = additionalData?.description;
    this.holiday = additionalData?.holiday ?? false;
    this.moving = additionalData?.moving ?? false;
    this.scope = additionalData?.scope;
    this.type = additionalData?.type;
  }

  /**
   * Convert most relevant information of day to string
   * @returns Stringified information of day
   */
  public override toString(): string {
    return `${getDayOfYear(this.date)} ${this.title}${
      this.moving ? ', moves' : ''
    }`;
  }
}

/**
 * A birthday
 */
export class Birthday extends SpecialDay {}

/**
 * A holiday
 */
export class Holiday extends CommemorativeDay {
  /**
   * Create a new holiday
   * @param title Title
   * @param date Date
   * @param sinceYear Since year
   * @param additionalData Additional data
   */
  constructor(
    title: string,
    date: Date,
    sinceYear?: number,
    additionalData?: Partial<
      Omit<CommemorativeDay, 'title' | 'date' | 'sinceYear' | 'holiday'>
    >,
  ) {
    super(title, date, sinceYear, additionalData);
    this.holiday = true;
  }
}

/**
 * A school holiday
 */
export class SchoolHoliday extends SpecialDay {
}
