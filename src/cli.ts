/**
 * Command line interface
 *
 * @module
 */
import { colors } from '@cliffy/ansi/colors';
import { Command } from '@cliffy/command';
import { HelpCommand } from '@cliffy/command/help';
import { Table } from '@cliffy/table';
import { format } from '@std/fmt/bytes';
import { execSync } from 'node:child_process';
import { mkdir, writeFile } from 'node:fs/promises';
import { dirname, resolve, sep } from 'node:path';
import { cwd, exit, platform } from 'node:process';
import denoJson from '../deno.json' with { type: 'json' };
import { logger, slugify } from './common.ts';
import { compilerModules, parserModules } from './modules.ts';
import type { SpecialDay } from './special-days.ts';

/**
 * Flags of command line interface
 */
export interface Flags {
  /**
   * Cache remote sources
   */
  cacheRemoteSources: boolean;
  /**
   * File name
   */
  fileName?: string;
  /**
   * Language
   */
  language: string;
  /**
   * Ignore cache
   */
  ignoreCache: boolean;
  /**
   * Open in browser
   */
  openInBrowser: boolean;
  /**
   * Parsers
   */
  parsers: string;
  /**
   * Year
   */
  year: number;
  /**
   * Additional flags
   */
  [k: string]: string | number | boolean | undefined;
}

/**
 * Default flags of command line interface
 */
export const defaultFlags: Flags = {
  cacheRemoteSources: false,
  ignoreCache: false,
  language: 'de',
  openInBrowser: false,
  parsers: 'wikipedia.de,easter,christmas,birthday.csv,holidays',
  year: new Date().getFullYear() + 1,
};

/**
 * Command line interface
 */
export const commandLineInterface: Command = new Command();

commandLineInterface
  .name('krlwlfrt-calendar')
  .version(denoJson.version);

commandLineInterface
  .command('help', new HelpCommand().global())
  .alias('h');

commandLineInterface
  .reset();

commandLineInterface
  .default('help');

commandLineInterface
  .reset();

commandLineInterface
  .command('compile')
  .alias('c')
  .description('compile a calendar')
  .arguments('<compiler:string>')
  .option('--cacheRemoteSources', 'Cache remote sources', {
    default: defaultFlags.cacheRemoteSources,
  })
  .option('--ignoreCache', 'Ignore cache', {
    default: defaultFlags.ignoreCache,
  })
  .option(
    '--parsers <parsers>',
    'List of parsers',
    {
      default: defaultFlags.parsers,
    },
  )
  .option('--fileName <fileName>', 'Filename for the calendar file')
  .option('--openInBrowser', 'Open resulting HTML in browser', {
    default: defaultFlags.openInBrowser,
  })
  .option('--year <year:number>', 'Year', {
    default: defaultFlags.year,
  })
  .option('--language <language>', 'Language', {
    default: defaultFlags.language,
  })
  .action(async (flags, compiler) => {
    const firstDay = new Date(flags.year, 0, 1);

    const specialDays: SpecialDay[] = [];

    const parsers = flags.parsers.toString().split(',');

    for (const parser of parsers) {
      const parserModule = parserModules.find((parserModule) =>
        dirname(parserModule.path).split(sep).pop() === parser
      );

      if (typeof parserModule === 'undefined') {
        logger.error(
          `Parser ${parser} does not exist. Please check list of loaded parsers and spelling.`,
        );
        continue;
      }

      specialDays.push(...await parserModule.exports.parse(firstDay, flags));
    }

    logger.info(`Gathered ${specialDays.length} special days.`);

    await mkdir(resolve(cwd(), 'build'), {
      recursive: true,
    });

    let fileName = `${compiler}-${JSON.stringify(flags)}`;
    if (
      typeof flags.fileName === 'string' &&
      flags.fileName.length > 0
    ) {
      fileName = flags.fileName;
    }

    const outputFile = resolve(
      cwd(),
      'build',
      `${slugify(fileName)}.html`,
    );

    const compilerModule = compilerModules.find((compilerModule) =>
      dirname(compilerModule.path).split(sep).pop() === compiler
    );

    if (typeof compilerModule === 'undefined') {
      logger.error(
        `Compiler ${compiler} does not exist. Please check list of loaded compilers and spelling.`,
      );
      exit(1);
    }

    const output = await compilerModule.exports.compile(
      firstDay,
      specialDays,
      flags,
    );

    await writeFile(outputFile, output);

    logger.info(`Wrote output to file file:///${outputFile}.`);

    if (platform === 'linux' && flags.openInBrowser) {
      execSync(`xdg-open ${outputFile}`);
    }
  });

commandLineInterface
  .command('list-parsers')
  .description('List parsers')
  .action(() => {
    const table = new Table();
    table.header(['Name', 'Path', 'Size'].map((title) => colors.bold(title)));

    for (const parserModule of parserModules) {
      table.push([
        colors.bold(dirname(parserModule.path).split(sep).pop()!),
        parserModule.path,
        format(parserModule.size),
      ]);
    }

    table.sort();

    console.log(`Possible values for --parsers:

${table.toString()}
`);
  });

commandLineInterface
  .command('list-compilers')
  .description('List compilers')
  .action(() => {
    const table = new Table();
    table.header(['Name', 'Path', 'Size'].map((title) => colors.bold(title)));

    for (const compilerModule of compilerModules) {
      table.push([
        colors.bold(dirname(compilerModule.path).split(sep).pop()!),
        compilerModule.path,
        format(compilerModule.size),
      ]);
    }

    table.sort();

    console.log(`Possible values for <compiler>:

${table.toString()}`);
  });
