/**
 * HTML compiler
 *
 * @module
 */

import { JSDOM } from 'jsdom';
import mustache from 'mustache';
import { existsSync } from 'node:fs';
import { readdir, readFile } from 'node:fs/promises';
import { basename, join, resolve } from 'node:path';
import { cwd, exit } from 'node:process';
import { commandLineInterface, type Flags } from '../../cli.ts';
import { getSource, logger } from '../../common.ts';
import {
  formatDate,
  getDaysInMonth,
  getDaysInYear,
  getSeason,
} from '../../datetime.ts';
import type { CompilerModuleFunction } from '../../modules.ts';
import {
  Birthday,
  CommemorativeDay,
  SchoolHoliday,
  SpecialDay,
} from '../../special-days.ts';

// TODO: replace mustache with https://www.npmjs.com/package/eta

interface HTMLCompilerFlags extends Flags {
  htmlVariant?: string;
  monthsOnly?: boolean;
}

const compileCommand = commandLineInterface.getCommand('compile')!;

compileCommand
  .option('--htmlVariant <htmlVariant>', 'HTML variant', {
    default: 'classic',
  })
  .option('--monthsOnly', 'Whether to output months only', {
    default: false,
  });

commandLineInterface
  .reset();

commandLineInterface
  .command('html-list-variants')
  .description('List HTML variants')
  .action(() => {
    console.log(`Possible values for --htmlVariant:

classic
perennial`);
  });

/**
 * Translations
 */
const translations: Record<string, {
  birthdays: string;
  calendar: string;
}> = {
  de: {
    birthdays: 'Geburtstage',
    calendar: 'Kalender',
  },
  en: {
    birthdays: 'birthdays',
    calendar: 'calendar',
  },
  sv: {
    birthdays: 'födelsedagar',
    calendar: 'kalender',
  },
};

/**
 * Season
 */
type Season = 'winter' | 'spring' | 'summer' | 'autumn';

/**
 * Data structure for usage in templates
 */
class CalendarDataStructure {
  /**
   * List of birthdays
   */
  birthdays: BirthdayDataStructure[] = [];
  /**
   * First half of birthdays
   */
  birthdaysFirstHalf: BirthdayDataStructure[] = [];
  /**
   * Second half of birthdays
   */
  birthdaysSecondHalf: BirthdayDataStructure[] = [];
  /**
   * Counts of types of days
   */
  counts = {
    average: 0,
    birthdays: 0,
    holidays: 0,
    movingDays: 0,
    schoolHolidays: 0,
    specialDays: 0,
  };
  /**
   * List of months
   */
  months: MonthDataStructure[] = [];
  /**
   * Paper CSS
   */
  paperCss = '';
  /**
   * Style for the whole calendar
   */
  style = '';
  /**
   * Translations
   */
  translations: unknown;
  /**
   * Year that the calendar is generated for
   */
  year = 0;
  /**
   * Whether to output months only
   */
  monthsOnly = false;
}

interface BirthdayDataStructure {
  /**
   * Day/date of the birthday
   */
  day: string;
  /**
   * Date of the day
   */
  date: Date;
  /**
   * Name of the person having the birthday
   */
  name: string;
}

/**
 * Data structure for a month
 */
class MonthDataStructure {
  /**
   * List of days in that month
   */
  days: DayDataStructure[] = [];

  /**
   * Description of month
   */
  description = '';

  /**
   * First half of days
   */
  firstHalfOfDays: DayDataStructure[] = [];

  /**
   * List of footnotes for that month
   */
  footnotes: string[] = [];

  /**
   * Name of that month
   */
  name: string;

  /**
   * Ordinal
   */
  ordinal: string;

  /**
   * Season of the month
   */
  season: Season;

  /**
   * Second half of days
   */
  secondHalfOfDays: DayDataStructure[] = [];

  constructor(name: string, season: Season, ordinal: string) {
    this.name = name;
    this.season = season;
    this.ordinal = ordinal;
  }
}

/**
 * A special day with metadata
 */
class _SpecialDayWithMetadata extends SpecialDay {
  /**
   * Classes to add to the special day
   */
  classes = '';
  /**
   * Footnode index for the special day
   */
  footnoteIndex?: number;
}

/**
 * Data structure for a day
 */
class DayDataStructure {
  /**
   * Classes for that day
   */
  classes = '';

  /**
   * Number of day in month
   */
  dayOfMonth: number;

  /**
   * Whether or not it is a holiday
   */
  holiday = false;

  /**
   * List of special days for that day
   */
  specialDays: _SpecialDayWithMetadata[] = [];

  constructor(dayOfMonth: number) {
    this.dayOfMonth = dayOfMonth;
  }
}

let footnoteCounter = 0;

/**
 * Add footnote for commemorative day
 * @param commemorativeDay Commemorative day to add footnote for/to
 * @param footnotes List of footnotes
 * @returns Number for footnote or undefined, if no description is supplied
 */
function addFootnote(
  commemorativeDay: CommemorativeDay,
  footnotes: string[],
): number | undefined {
  if (
    typeof commemorativeDay.description !== 'string' ||
    commemorativeDay.description.length === 0
  ) {
    return;
  }

  const footnoteIndex = ++footnoteCounter;

  footnotes.push(`[${footnoteIndex}] ${commemorativeDay.description}`);

  return footnoteIndex;
}

/**
 * Compile a list of special days to an HTML calendar
 * @param firstDay First day of year
 * @param specialDays List of special days
 * @returns A promise that resolves with an HTML string compiled from the supplied list of special days
 */
export const compile: CompilerModuleFunction = async function (
  firstDay: Date,
  specialDays: SpecialDay[],
  flags: HTMLCompilerFlags,
): Promise<string> {
  const pathToTemplates = resolve(
    cwd(),
    'src',
    'compilers',
    'html',
    'templates',
  );
  const templates: Record<string, string> = {};

  let htmlVariant = flags.htmlVariant ?? 'classic';

  if (htmlVariant.includes('..')) {
    logger.error(
      'HTML variant is not supposed to contain any directory traversals.',
    );

    exit(1);
  }

  if (!existsSync(resolve(pathToTemplates, htmlVariant))) {
    logger.warn(
      `Template '${flags.htmlVariant}' does not exist! Using 'classic' instead'.`,
    );

    htmlVariant = 'classic';
  }

  for (const file of await readdir(join(pathToTemplates, htmlVariant))) {
    if (!/\.html\.mustache$/.test(join(pathToTemplates, htmlVariant, file))) {
      continue;
    }

    const buffer = await readFile(join(pathToTemplates, htmlVariant, file));
    templates[basename(file, '.html.mustache')] = buffer.toString();
  }

  logger.info(`Loaded templates: ${Object.keys(templates)}`);

  // build data structure for templates
  const structureForTemplate = new CalendarDataStructure();

  let buffer = await readFile(
    join(cwd(), 'node_modules', 'paper-css', 'paper.css'),
  );
  structureForTemplate.paperCss = buffer.toString();

  buffer = await readFile(join(pathToTemplates, htmlVariant, 'style.css'));
  structureForTemplate.style = buffer.toString();

  const language = flags.language ?? 'de';
  structureForTemplate.translations = translations[language];
  structureForTemplate.year = firstDay.getFullYear();

  const numberOfMonthsInYear = 12;

  for (let monthIdx = 0; monthIdx < numberOfMonthsInYear; monthIdx++) {
    const monthFirstDay = new Date(firstDay.getFullYear(), monthIdx, 1);

    const numberOfDaysInMonth = getDaysInMonth(
      monthFirstDay.getFullYear(),
      monthFirstDay.getMonth(),
    );

    const days: DayDataStructure[] = [];
    const firstHalfOfDays: DayDataStructure[] = [];
    const secondHalfOfDays: DayDataStructure[] = [];

    for (let day = 1; day <= numberOfDaysInMonth; day++) {
      let classes = '';
      if (new Date(monthFirstDay.getFullYear(), monthIdx, day).getDay() === 0) {
        classes = 'sunday';
      }

      const dayData = new DayDataStructure(day);
      dayData.classes = classes;

      days.push(dayData);

      if (day <= Math.ceil(numberOfDaysInMonth / 2)) {
        firstHalfOfDays.push(dayData);
      } else {
        secondHalfOfDays.push(dayData);
      }
    }

    const monthData = new MonthDataStructure(
      formatDate(monthFirstDay, language, {
        month: 'long',
      }),
      getSeason(monthFirstDay),
      formatDate(monthFirstDay, language, {
        month: '2-digit',
      }),
    );
    monthData.days = days;
    monthData.firstHalfOfDays = firstHalfOfDays;
    monthData.secondHalfOfDays = secondHalfOfDays;

    if (htmlVariant === 'perennial') {
      let monthName = formatDate(
        monthFirstDay,
        language,
        { month: 'long' },
      );

      if (
        language === 'sv' && monthName === 'mars'
      ) {
        monthName += ' (månad)';
      }

      monthName = `${monthName.substring(0, 1).toUpperCase()}${
        monthName.substring(1)
      }`;

      const source = await getSource(
        join(
          cwd(),
          'resources',
          `wikipedia.${language}`,
          `${monthName}.html`,
        ),
        `https://${language}.wikipedia.org/wiki/${monthName}`,
        flags,
      );

      const dom = new JSDOM(source);

      for (
        const element of dom.window.document.querySelectorAll(
          'div.mw-parser-output > *',
        ).values()
      ) {
        if (element.nodeName === 'P') {
          monthData.description += element.textContent;
        }
        if (element.nodeName === 'DIV' && element.id === 'toc') {
          break;
        }
      }
      monthData.description = monthData.description.replace(/\[[0-9]]/g, '');
    }

    structureForTemplate.months.push(monthData);
  }

  for (const specialDay of specialDays) {
    let classes = '';
    let footnoteIndex;

    if (specialDay instanceof Birthday) {
      structureForTemplate.counts.birthdays++;

      classes += ' birthday';

      if (typeof specialDay.sinceYear === 'number') {
        if (htmlVariant === 'perennial') {
          specialDay.title += ` ('${
            specialDay.sinceYear.toString(10).substring(2)
          })`;
        } else {
          specialDay.title += ` (${
            firstDay.getFullYear() - specialDay.sinceYear
          })`;
        }
      }

      structureForTemplate.birthdays.push({
        day: formatDate(specialDay.date, language, {
          day: '2-digit',
          month: '2-digit',
        }),
        date: specialDay.date,
        name: specialDay.title,
      });
    }

    if (specialDay instanceof CommemorativeDay) {
      structureForTemplate.counts.specialDays++;

      footnoteIndex = addFootnote(
        specialDay,
        structureForTemplate.months[specialDay.date.getMonth()].footnotes,
      );

      if (specialDay.moving) {
        classes += ' moving';
        structureForTemplate.counts.movingDays++;
      }

      if (specialDay.holiday) {
        classes += ' sunday';

        if (
          !structureForTemplate.months[specialDay.date.getMonth()]
            .days[specialDay.date.getDate() - 1].holiday
        ) {
          structureForTemplate.counts.holidays++;

          structureForTemplate.months[specialDay.date.getMonth()]
            .days[specialDay.date.getDate() - 1].holiday = true;
        }
      }
    }

    if (specialDay instanceof SchoolHoliday) {
      structureForTemplate.counts.schoolHolidays++;

      structureForTemplate.months[specialDay.date.getMonth()]
        .days[specialDay.date.getDate() - 1].holiday = true;

      continue;
    }

    structureForTemplate.months[specialDay.date.getMonth()]
      .days[specialDay.date.getDate() - 1].specialDays.push({
        ...specialDay,
        classes: classes,
        footnoteIndex,
      });
  }

  structureForTemplate.months.forEach((month) => {
    month.days.forEach((day) => {
      day.specialDays.sort((a, b) => {
        return a.title.localeCompare(b.title);
      });
    });
  });

  structureForTemplate.birthdays.sort((a, b) => {
    if (a.date.getTime() === b.date.getTime()) {
      return a.name.localeCompare(b.name);
    }

    return a.date.getTime() - b.date.getTime();
  });

  structureForTemplate.birthdays.forEach((birthday, birthdayIdx) => {
    if (birthdayIdx % 2 === 0) {
      structureForTemplate.birthdaysFirstHalf.push(birthday);
    } else {
      structureForTemplate.birthdaysSecondHalf.push(birthday);
    }
  });

  structureForTemplate.counts.average = Math.round(
    structureForTemplate.counts.specialDays /
      getDaysInYear(firstDay.getFullYear()) *
      100,
  ) / 100;

  structureForTemplate.monthsOnly = flags.monthsOnly ?? false;

  return mustache.render(templates.calendar, structureForTemplate, templates);
};
