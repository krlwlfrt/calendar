import type { Module } from '@krlwlfrt/dml';
import type { Flags } from './cli.ts';
import type { SpecialDay } from './special-days.ts';

/**
 * Compile
 * @param firstDay First day of year
 * @param specialDays Special days
 */
export interface ParserModuleFunction {
  (firstDay: Date, flags: Flags): Promise<SpecialDay[]>;
}

/**
 * A parser module
 */

export interface ParserModule extends Module {
  /**
   * Exports of module
   */
  exports: {
    parse: ParserModuleFunction;
  };
}

/**
 * Compile
 * @param firstDay First day of year
 * @param specialDays Special days
 */
export type CompilerModuleFunction = (
  firstDay: Date,
  specialDays: SpecialDay[],
  flags: Flags,
) => Promise<string>;

/**
 * A compiler module
 */
export interface CompilerModule extends Module {
  /**
   * Exports of module
   */
  exports: {
    compile: CompilerModuleFunction;
  };
}

/**
 * Array of parser modules
 */
export const parserModules: ParserModule[] = [];

/**
 * Array of compiler modules
 */
export const compilerModules: CompilerModule[] = [];
