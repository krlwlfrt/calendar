/**
 * "Parser" for days related to christmas
 *
 * @module
 */

import type { Flags } from '../../cli.ts';
import { logger } from '../../common.ts';
import type { ParserModuleFunction } from '../../modules.ts';
import { CommemorativeDay } from '../../special-days.ts';

// TODO: translate titles

/**
 * Get special days related to christmas
 * @param firstDay First day of the year to get specials days related to christmas for
 * @param _flags Command line interface flags
 * @returns An array of special days related to christmas
 */
export const parse: ParserModuleFunction = function (
  firstDay: Date,
  _flags: Flags,
): Promise<CommemorativeDay[]> {
  logger.info(`Parsing special days related to christmas.`);

  const christmasDays: CommemorativeDay[] = [];

  // get date of Christmas Eve
  const christmasEve = new Date(firstDay.getFullYear(), 11, 25);

  // get date of fourth advent - subtract day of week, to go to previous sunday
  const fourthAdvent = new Date(
    firstDay.getFullYear(),
    11,
    25 - christmasEve.getDay(),
  );

  christmasDays.push(
    new CommemorativeDay('4. Advent', fourthAdvent, undefined, {
      moving: true,
    }),
  );

  christmasDays.push(
    new CommemorativeDay(
      '3. Advent',
      new Date(
        fourthAdvent.getFullYear(),
        fourthAdvent.getMonth(),
        fourthAdvent.getDate() - 1 * 7,
      ),
      undefined,
      { moving: true },
    ),
  );

  christmasDays.push(
    new CommemorativeDay(
      '2. Advent',
      new Date(
        fourthAdvent.getFullYear(),
        fourthAdvent.getMonth(),
        fourthAdvent.getDate() - 2 * 7,
      ),
      undefined,
      { moving: true },
    ),
  );

  christmasDays.push(
    new CommemorativeDay(
      '1. Advent',
      new Date(
        fourthAdvent.getFullYear(),
        fourthAdvent.getMonth(),
        fourthAdvent.getDate() - 3 * 7,
      ),
      undefined,
      { moving: true },
    ),
  );

  christmasDays.push(
    new CommemorativeDay(
      'Totensonntag (Ewigkeitssonntag)',
      new Date(
        fourthAdvent.getFullYear(),
        fourthAdvent.getMonth(),
        fourthAdvent.getDate() - 4 * 7,
      ),
      undefined,
      { moving: true },
    ),
  );

  christmasDays.push(
    new CommemorativeDay(
      'Volkstrauertag',
      new Date(
        fourthAdvent.getFullYear(),
        fourthAdvent.getMonth(),
        fourthAdvent.getDate() - 5 * 7,
      ),
      undefined,
      { moving: true },
    ),
  );

  logger.info(
    `Parsed ${christmasDays.length} special days related to christmas.`,
  );

  return Promise.resolve(christmasDays);
};
