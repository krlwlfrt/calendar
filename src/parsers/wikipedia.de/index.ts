/**
 * Parser for comemmorative days from de.wikipedia.org
 *
 * @module
 * @see https://de.wikipedia.org/wiki/Liste_von_Gedenk-_und_Aktionstagen
 */

import { JSDOM } from 'jsdom';
import { resolve } from 'node:path';
import { cwd } from 'node:process';
import type { Flags } from '../../cli.ts';
import { getSource, logger, stripTags } from '../../common.ts';
import { getSpecificDaysInMonth } from '../../datetime.ts';
import type { ParserModuleFunction } from '../../modules.ts';
import { CommemorativeDay, type SpecialDay } from '../../special-days.ts';

/**
 * German months (lowercase)
 */
type Month =
  | 'januar'
  | 'februar'
  | 'märz'
  | 'april'
  | 'mai'
  | 'juni'
  | 'juli'
  | 'august'
  | 'oktober'
  | 'november'
  | 'dezember';

/**
 * German weekdays (lowercase)
 */
type Weekday =
  | 'montag'
  | 'dienstag'
  | 'mittwoch'
  | 'donnerstag'
  | 'freitag'
  | 'samstag'
  | 'sonntag';

/**
 * Map from german weekday name to index
 */
const weekdayMap = {
  dienstag: 2,
  donnerstag: 4,
  freitag: 5,
  mittwoch: 3,
  montag: 1,
  samstag: 6,
  sonntag: 0,
};

/**
 * Map from german month name to index
 */
const monthMap = {
  april: 3,
  august: 7,
  dezember: 11,
  februar: 1,
  januar: 0,
  juli: 6,
  juni: 5,
  mai: 4,
  märz: 2,
  november: 10,
  oktober: 9,
  september: 8,
};

/**
 * Map from german ordinal name to index
 */
const ordinalMap = {
  dritter: 2,
  erster: 0,
  fünfter: 4,
  vierter: 3,
  zweiter: 1,
};

/**
 * Get the item represented by a german ordinal name from a list
 * @param ordinal German ordinal name
 * @param list List to get item from
 * @returns The item from the list that is represented by the given german ordinal
 */
function getOrdinalFromList<T>(ordinal: string, list: T[]): T {
  if (!(ordinal in ordinalMap) && ordinal !== 'letzter') {
    throw new Error(`Invalid ordinal ${ordinal}!`);
  }

  let idx = -1;

  if (ordinal === 'letzter') {
    idx = list.length - 1;
  } else {
    idx = ordinalMap[ordinal as keyof typeof ordinalMap];
  }

  if (idx > list.length - 1) {
    throw new Error(`Provided list has no ${ordinal}!`);
  }

  return list[idx];
}

/**
 * Adjust scope of commemorative day
 * @param scope Scope to adjust
 * @returns Adjusted scope of ccommemorative day
 */
function adjustScope(scope: string | undefined): string | undefined {
  if (typeof scope === 'undefined') {
    return;
  }

  let adjustedScope = scope.replace(/<span[^>]*>[^<]*<\/span>/g, '');

  adjustedScope = adjustedScope.replace(/<br\s?\/?>/, '||');

  adjustedScope = stripTags(adjustedScope);

  adjustedScope = adjustedScope.replace(/&nbsp;/g, ' ');

  adjustedScope = adjustedScope.replace(
    /(international|römisch-katholische Kirche|römisch-katholisch|christlich|gregorianischer kalender|europa|\/)/gi,
    '',
  );

  adjustedScope = adjustedScope.split('||')
    .map((part) => part.trim())
    .join(', ');

  adjustedScope = adjustedScope.trim();

  return adjustedScope;
}

/**
 * Adjust description of commemorative day
 * @param description Description to adjust
 * @returns Adjusted description of commemorative day
 */
function adjustDescription(
  description: string | undefined,
): string | undefined {
  if (typeof description === 'undefined') {
    return;
  }

  let adjustedDescription = description.replace(/\[[0-9]+]/g, ' ');

  adjustedDescription = adjustedDescription.replace('&nbsp;', ' ');

  adjustedDescription = adjustedDescription.replace(/<br\s?\/?>/, '||');

  adjustedDescription = stripTags(adjustedDescription);

  adjustedDescription = adjustedDescription.split('||')
    .map((part) => part.trim())
    .join(', ');

  adjustedDescription = adjustedDescription.trim();

  if (
    [
      'traditionell',
      'uno',
      'bundesrat',
      'unesco',
      'unicef',
      'who, unaids',
      'who',
      'iof, who',
      'vereinte nationen',
      'vormals britisch',
      'nicht staatlich',
      'UN-Statistikkommission',
      'staatsfeiertag',
      'who, fao, oie',
      'unwto',
      'europarat',
      'dlr und esa',
    ].indexOf(adjustedDescription.toLowerCase()) >= 0
  ) {
    return;
  }

  if (adjustedDescription.match(/^[0-9]+$/) !== null) {
    return;
  }

  return adjustedDescription;
}

/**
 * Parse commemorative days from month tables
 * @param dom DOM to parse month tables from
 * @param firstDay First day of the year to parse commemorative days for
 * @returns An array of commemorative days
 */
function parseMonthTables(
  dom: JSDOM,
  firstDay: Date,
): CommemorativeDay[] {
  const commemorativeDays: CommemorativeDay[] = [];

  const monthTables: NodeListOf<Element> = dom.window.document.querySelectorAll(
    'table.wikitable.sortable',
  );

  const expectedNumberOfCells = 5;

  const typeCellIdx = 1;
  const scopeCellIdx = 2;
  const titleCellIdx = 3;
  const sinceYearCellIdx = 4;
  const descriptionCellIdx = 5;

  const daysInMonth = 31;

  monthTables.forEach((monthTable, monthIdx) => {
    const tableRows = monthTable.querySelectorAll('tr');

    tableRows.forEach((tableRow) => {
      const cells = tableRow.querySelectorAll('td');

      if (cells.length < expectedNumberOfCells) {
        return;
      }

      let typeCell: string | undefined;
      let scopeCell: string | undefined;
      let sinceYearCell: number | undefined;
      let descriptionCell: string | undefined;

      let commemorativeDay: CommemorativeDay;

      if (cells.item(typeCellIdx).innerHTML !== '') {
        typeCell = stripTags(
          cells.item(typeCellIdx).innerHTML
            .trim(),
        );
      }

      if (cells.item(scopeCellIdx).innerHTML !== '') {
        scopeCell = cells.item(scopeCellIdx).innerHTML;
      }

      if (cells.item(sinceYearCellIdx).innerHTML !== '') {
        sinceYearCell = parseInt(
          cells.item(sinceYearCellIdx).innerHTML
            .trim(),
          10,
        );
      }

      if (
        cells.item(descriptionCellIdx) !== null &&
        cells.item(descriptionCellIdx).innerHTML !== ''
      ) {
        descriptionCell = stripTags(
          cells
            .item(descriptionCellIdx)
            .innerHTML
            .trim(),
        );
      }

      const titleCell = stripTags(
        cells.item(titleCellIdx).innerHTML
          .replace(/<br\s?\/?>/, ' '),
      )
        .replace(/\[[0-9]+]/g, ' ')
        .trim();
      const dateCell = stripTags(
        cells.item(0).innerHTML
          .trim(),
      )
        .replace('&nbsp;', ' ')
        .toLowerCase()
        .trim();

      const dateExpressionMatch = dateCell.match(
        /^([0-9]{1,2})\.?\s([a-zäöü]*)$/,
      );
      const monthMatchIdx = 2;

      if (dateExpressionMatch !== null) {
        const dayOfMonth = parseInt(dateExpressionMatch[1], 10);
        if (
          dayOfMonth < 1 || dayOfMonth > daysInMonth || Object.keys(monthMap)
              .indexOf(dateExpressionMatch[monthMatchIdx]) < 0
        ) {
          logger.warn(
            `Could not find a match for "${dateCell}" in month table ${monthIdx}.`,
          );

          return;
        }

        commemorativeDay = new CommemorativeDay(
          titleCell,
          new Date(
            firstDay.getFullYear(),
            monthMap[dateExpressionMatch[monthMatchIdx] as Month],
            parseInt(dateExpressionMatch[1], 10),
          ),
          sinceYearCell,
          {
            type: typeCell,
            scope: adjustScope(scopeCell),
            description: adjustDescription(descriptionCell),
          },
        );

        commemorativeDays.push(commemorativeDay);
      } else {
        const movingDateExpressionMatch = dateCell.match(
          /^(erster|zweiter|dritter|vierter|fünfter|letzter) (montag|dienstag|mittwoch|donnerstag|freitag|samstag|sonntag)$/,
        );
        const weekdayMatchIdx = 2;

        if (movingDateExpressionMatch !== null) {
          commemorativeDay = new CommemorativeDay(
            titleCell,
            getOrdinalFromList(
              movingDateExpressionMatch[1],
              getSpecificDaysInMonth(
                firstDay.getFullYear(),
                monthIdx,
                weekdayMap[
                  movingDateExpressionMatch[weekdayMatchIdx] as Weekday
                ],
              ),
            ),
            sinceYearCell,
            {
              type: typeCell,
              scope: adjustScope(scopeCell),
              description: adjustDescription(descriptionCell),
              moving: true,
            },
          );

          commemorativeDays.push(commemorativeDay);
        } else {
          logger.warn(
            `Could not find a match for moving date description "${dateCell}" in month table ${monthIdx}.`,
          );
        }
      }
    });
  });

  return commemorativeDays;
}

/**
 * Parse commemorative days from german Wikipedia
 * @param firstDay First day of the year to parse commemorative days for
 * @returns Promise that resolves with an array of special days
 */
export const parse: ParserModuleFunction = async function (
  firstDay: Date,
  flags: Flags,
): Promise<SpecialDay[]> {
  logger.info(`Parsing special days from Wikipedia.`);

  const commemorativeDays: CommemorativeDay[] = [];

  const source = await getSource(
    resolve(
      cwd(),
      'resources',
      'wikipedia.de',
      `${firstDay.getFullYear()}.html`,
    ),
    'https://de.wikipedia.org/wiki/Liste_von_Gedenk-_und_Aktionstagen',
    flags,
  );

  const dom = new JSDOM(source);

  commemorativeDays.push(...parseMonthTables(dom, firstDay));

  logger.info(
    `Parsed ${commemorativeDays.length} special days from Wikipedia.`,
  );

  return commemorativeDays;
};
