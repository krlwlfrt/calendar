/**
 * Parser for (school) holidays
 *
 * @module
 * @see https://www.ferienwiki.de/
 */

import { JSDOM } from 'jsdom';
import { resolve } from 'node:path';
import { cwd } from 'node:process';
import { commandLineInterface, type Flags } from '../../cli.ts';
import { getSource, logger, stripTags } from '../../common.ts';
import { getDaysInYear, millisecondsPerDay } from '../../datetime.ts';
import type { ParserModuleFunction } from '../../modules.ts';
import { Holiday, SchoolHoliday, type SpecialDay } from '../../special-days.ts';

interface HolidaysParserFlags extends Flags {
  state?: string;
  includeSchoolHolidays?: boolean;
}

const compileCommand = commandLineInterface.getCommand('compile')!;

compileCommand
  .option('--state <state>', 'State')
  .option('--includeSchoolHolidays', 'Include school holidays', {
    default: false,
  });

/**
 * Get school holidays
 * @param firstDay First day of year
 * @param year Year to get school holidays for
 * @param flags Command line interface flags
 * @returns A promise that resolves with an array of school holidays
 */
async function getSchoolHolidays(
  firstDay: Date,
  year: number,
  flags: HolidaysParserFlags & { state: string },
): Promise<SchoolHoliday[]> {
  logger.info(`Parsing school holidays in ${flags.state} for ${year}.`);

  const schoolHolidays: SchoolHoliday[] = [];

  const source = await getSource(
    resolve(cwd(), 'resources', 'ferienwiki', `ferien-${year}.html`),
    `https://www.ferienwiki.de/ferienkalender/${year}/de`,
    flags,
  );

  const dom = new JSDOM(source);

  const table = dom.window.document.querySelectorAll(
    'table.table.table-bordered.table-striped.table-hover',
  )[0];

  const rows = table.querySelectorAll('tr');

  let stateFound = false;

  const states: string[] = [];

  for (const row of rows) {
    const cells = row.querySelectorAll('td');

    if (cells.length === 0) {
      continue;
    }

    const stateCell = stripTags(cells[0].innerHTML)
      .toLowerCase()
      .trim();

    states.push(stateCell);

    if (stateCell === flags.state) {
      stateFound = true;

      let cellIdx = 0;

      for (const cell of cells) {
        if (cellIdx++ === 0) {
          continue;
        }

        if (/^\s*$/.test(cell.innerHTML)) {
          logger.info('Cell is empty. Skipping...');
          continue;
        }

        const schoolHolidayDates = stripTags(cell.innerHTML)
          .toLowerCase()
          .replace(/\s/g, '')
          .split(',');

        for (const schoolHolidayDate of schoolHolidayDates) {
          if (schoolHolidayDate === '-') {
            continue;
          }

          const [begin, end] = schoolHolidayDate.split('-');

          const [beginDate, beginMonth] = begin.split('.');

          let iteratorDate = new Date(
            year,
            parseInt(beginMonth) - 1,
            parseInt(beginDate),
          );

          if (year === firstDay.getFullYear()) {
            schoolHolidays.push(
              new SchoolHoliday('Schulferien', new Date(iteratorDate)),
            );
          }

          if (typeof end !== 'undefined') {
            const [endDate, endMonth] = end.split('.');

            let i = 0;

            do {
              iteratorDate = new Date(
                iteratorDate.getTime() + millisecondsPerDay,
              );

              if (iteratorDate.getFullYear() !== firstDay.getFullYear()) {
                continue;
              }

              schoolHolidays.push(
                new SchoolHoliday('Schulferien', new Date(iteratorDate)),
              );
            } while (
              !(
                iteratorDate.getDate() === parseInt(endDate) &&
                iteratorDate.getMonth() === parseInt(endMonth) - 1
              ) && i++ < getDaysInYear(year)
            );
          }
        }
      }
    }
  }

  if (!stateFound) {
    logger.error(
      `State '${flags.state}' could not be found! Possible states are ${
        states.join(', ')
      }.`,
    );
  }

  logger.info(
    `Parsed ${schoolHolidays.length} school holidays in ${flags.state} for ${year}.`,
  );

  return schoolHolidays;
}

/**
 * Get holidays for a state
 * @param firstDay First day of year
 * @param state State to get holidays for
 * @param flags Command line interface flags
 * @returns An array of holidays for a state
 */
async function getHolidays(
  firstDay: Date,
  flags: HolidaysParserFlags & { state: string },
): Promise<SpecialDay[]> {
  logger.info(`Parsing holidays in ${flags.state}.`);

  const holidays: SpecialDay[] = [];

  const source = await getSource(
    resolve(
      cwd(),
      'resources',
      'ferienwiki',
      `feiertage-${firstDay.getFullYear()}.html`,
    ),
    `https://www.ferienwiki.de/feiertage/${firstDay.getFullYear()}/de`,
    flags,
  );

  const dom = new JSDOM(source);

  const table = dom.window.document.querySelectorAll(
    'table.table.table-striped.table-hover',
  )[0];

  const rows = table.querySelectorAll('tr');

  const statesCellIdx = 2;

  rows.forEach((row: HTMLTableRowElement) => {
    const cells = row.querySelectorAll('td');

    if (cells.length === 0) {
      return;
    }

    const states = stripTags(cells[statesCellIdx].innerHTML)
      .toLowerCase()
      .replace(' und ', ',')
      .split(',')
      .map((item) => item.trim());

    if (
      states.indexOf(flags.state) >= 0 ||
      states.indexOf('alle bundesländer') >= 0
    ) {
      const title = stripTags(cells[0].innerHTML)
        .trim();
      const dateMatch = cells[1].innerHTML.trim()
        .match(/([0-9]{2})\.([0-9]{2})\.([0-9]{4})/);
      const monthMatchIdx = 2;

      if (Array.isArray(dateMatch)) {
        const holiday = new Holiday(
          title,
          new Date(
            firstDay.getFullYear(),
            parseInt(dateMatch[monthMatchIdx]) - 1,
            parseInt(dateMatch[1]),
          ),
        );

        holiday.holiday = true;

        holidays.push(holiday);
      }
    }
  });

  logger.info(`Parsed ${holidays.length} holidays for ${flags.state}.`);

  return holidays;
}

/**
 * Parse data from Ferienwiki page
 * @param firstDay First day of year
 * @param flags Command line interface flags
 * @returns An array of holidays and optionally school holidays
 */
export const parse: ParserModuleFunction = async function (
  firstDay: Date,
  flags: HolidaysParserFlags,
): Promise<SpecialDay[]> {
  const holidays: SpecialDay[] = [];

  if (typeof flags.state !== 'undefined') {
    if (flags.includeSchoolHolidays) {
      // parse school holidays from year before, because holidays that include the new year are listed in the year before
      holidays.push(
        ...await getSchoolHolidays(
          firstDay,
          firstDay.getFullYear() - 1,
          // @ts-expect-error: just checked above, that state is set
          flags,
        ),
      );
      holidays.push(
        ...await getSchoolHolidays(
          firstDay,
          firstDay.getFullYear(),
          // @ts-expect-error: just checked above, that state is set
          flags,
        ),
      );
    }

    holidays.push(
      ...await getHolidays(
        firstDay,
        // @ts-expect-error: just checked above, that state is set
        flags,
      ),
    );
  } else {
    logger.error(
      `Can not include holidays without a state. Use option 'state' to supply a state.`,
    );
  }

  return holidays;
};
