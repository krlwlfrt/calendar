/**
 * "Parser" for days related to easter date
 *
 * @module
 */

import { easter } from 'date-easter';
import type { Flags } from '../../cli.ts';
import { logger } from '../../common.ts';
import { formatDate, millisecondsPerDay } from '../../datetime.ts';
import type { ParserModuleFunction } from '../../modules.ts';
import { CommemorativeDay } from '../../special-days.ts';

// TODO: translate titles

/**
 * Array of days depending on easter
 */
export const daysDependingOnEaster: {
  title: string;
  daysRelativeToEaster: number;
  additionalData?: Partial<
    Omit<CommemorativeDay, 'title' | 'date' | 'sinceYear'>
  >;
}[] = [
  {
    title: 'Weiberfastnacht',
    daysRelativeToEaster: -52,
  },
  {
    title: 'Schmotziger Donnerstag',
    daysRelativeToEaster: -52,
  },
  {
    title: 'Rosenmontag',
    daysRelativeToEaster: -48,
  },
  {
    title: 'Faschingsdienstag',
    daysRelativeToEaster: -47,
  },
  {
    title: 'Aschermittwoch',
    daysRelativeToEaster: -46,
  },
  {
    title: 'Basler Fasnacht',
    daysRelativeToEaster: -41,
  },
  {
    title: 'Basler Fasnacht',
    daysRelativeToEaster: -40,
  },
  {
    title: 'Basler Fasnacht',
    daysRelativeToEaster: -39,
  },
  {
    title: 'Palmsonttag',
    daysRelativeToEaster: -7,
  },
  {
    title: 'Gründonnerstag',
    daysRelativeToEaster: -3,
  },
  {
    title: 'Karfreitag',
    daysRelativeToEaster: -3,
    additionalData: {
      holiday: true,
    },
  },
  {
    title: 'Karsamstag',
    daysRelativeToEaster: -1,
  },
  {
    title: 'Ostersonntag',
    daysRelativeToEaster: 0,
    additionalData: {
      holiday: true,
    },
  },
  {
    title: 'Ostermontag',
    daysRelativeToEaster: 1,
    additionalData: {
      holiday: true,
    },
  },
  {
    title: 'Weißer Sonntag',
    daysRelativeToEaster: 7,
  },
  {
    title: 'Christi Himmelfahrt',
    daysRelativeToEaster: 39,
    additionalData: {
      holiday: true,
    },
  },
  {
    title: 'Vatertag',
    daysRelativeToEaster: 39,
  },
  {
    title: 'Welltag der sozialen Kommunikationsmittel',
    daysRelativeToEaster: 42,
  },
  {
    title: 'Pfingstsonntag',
    daysRelativeToEaster: 49,
    additionalData: {
      holiday: true,
    },
  },
  {
    title: 'Pfingstmontag',
    daysRelativeToEaster: 50,
    additionalData: {
      holiday: true,
    },
  },
  {
    title: 'Trinitatis',
    daysRelativeToEaster: 56,
  },
  {
    title: 'Fronleichnam',
    daysRelativeToEaster: 60,
  },
];

/**
 * Get special days related to easter
 * @param firstDay First day of the year to get special days related to easter for
 * @param Flags Command line interface flags
 * @returns A list of special days related to easter
 */
export const parse: ParserModuleFunction = function (
  firstDay: Date,
  flags: Flags,
): Promise<CommemorativeDay[]> {
  logger.info('Parsing special days related to easter.');

  const commemorativeDays: CommemorativeDay[] = [];

  const easterDate = easter(firstDay.getFullYear());

  const easterSunday = new Date(
    easterDate.year,
    easterDate.month - 1,
    easterDate.day,
  );

  logger.info(
    `Using easter date ${
      formatDate(
        easterSunday,
        flags.language,
        {
          day: '2-digit',
          month: '2-digit',
          year: 'numeric',
        },
      )
    }.`,
  );

  for (const dayDependingOnEaster of daysDependingOnEaster) {
    commemorativeDays.push(
      new CommemorativeDay(
        dayDependingOnEaster.title,
        new Date(
          easterSunday.getTime() +
            millisecondsPerDay * dayDependingOnEaster.daysRelativeToEaster,
        ),
        undefined,
        {
          moving: true,
          ...dayDependingOnEaster.additionalData,
        },
      ),
    );
  }

  logger.info(`Parsed ${commemorativeDays.length} days related to easter.`);

  return Promise.resolve(commemorativeDays);
};
