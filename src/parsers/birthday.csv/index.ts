/**
 * Parser for birthdays
 *
 * @module
 */

import { existsSync } from 'node:fs';
import { readdir, readFile } from 'node:fs/promises';
import { basename, join } from 'node:path';
import { cwd } from 'node:process';
import { commandLineInterface, defaultFlags, type Flags } from '../../cli.ts';
import { logger } from '../../common.ts';
import { formatDate } from '../../datetime.ts';
import type { ParserModuleFunction } from '../../modules.ts';
import { Birthday } from '../../special-days.ts';

/**
 * Birthday CSV parser flags
 */
interface BirthdayCsvParserFlags extends Flags {
  /**
   * Birthday file
   */
  birthdayFile?: string;
}

// add --birthdayFile option to compile command
commandLineInterface
  .getCommand('compile')!
  .option('--birthdayFile <birthdayFile>', 'Birthday file', {
    default: 'example',
  });

commandLineInterface
  .reset()
  .command('birthday-csv-sort')
  .arguments('<birthdayFile:string>')
  .description('Sort birthday file')
  .option('--by <by:string>', 'Sort by `name` or `date`', {
    default: 'date',
  })
  .action(async (flags, birthdayFile) => {
    logger.level = 'error';

    // parse birthdays
    const birthdays = await parse(
      new Date(new Date().getFullYear(), 0, 1),
      {
        ...flags,
        ...defaultFlags,
        ...{
          birthdayFile,
        },
      },
    );

    let longestTitleLength = 0;

    const csv = birthdays
      .sort((a, b) => {
        // sort by name
        if (flags.by === 'name') {
          return a.title.localeCompare(b.title);
        }

        // check for longest title
        if (a.title.length > longestTitleLength) {
          longestTitleLength = a.title.length;
        }

        // check for longest title
        if (b.title.length > longestTitleLength) {
          longestTitleLength = a.title.length;
        }

        // sort by date
        const timeDifference = a.date.getTime() - b.date.getTime();

        // use name/title as fallback
        return timeDifference !== 0
          ? timeDifference
          : a.title.localeCompare(b.title);
      })
      .map((specialDay) => {
        // set formatting options
        const format: Intl.DateTimeFormatOptions = {
          day: '2-digit',
          month: '2-digit',
        };

        // add year, if it was present
        if (typeof specialDay.sinceYear !== 'undefined') {
          specialDay.date.setFullYear(specialDay.sinceYear);
          format.year = 'numeric';
        }

        // format date
        const formattedDate = formatDate(specialDay.date, 'de', format);

        // set start of line
        const line = `${specialDay.title}, `;

        // return padded line for equal length
        return `${line.padEnd(longestTitleLength + 2, ' ')}${formattedDate}`;
      })
      .join('\n');

    console.log(csv);
  });

commandLineInterface
  .reset()
  .command('birthday-csv-list')
  .description('List birthday files')
  .action(async () => {
    const list = await readdir(join(cwd(), 'resources', 'birthday.csv'));

    const printableList = list
      .map((file) => basename(file, '.csv'))
      .join('\n');

    console.log(`Possible values for --birthdayFile:

${printableList}

Put your birthday files into '${join(cwd(), 'resources', 'birthday.csv')}'.`);
  });

/**
 * Parse birthdays
 * @param firstDay First day of year
 * @param flags Command line interface flags
 * @returns A promise that resolves with an array of birthdays
 */
export const parse: ParserModuleFunction = async function (
  firstDay: Date,
  flags: BirthdayCsvParserFlags,
): Promise<Birthday[]> {
  logger.info(`Parsing list of birthdays.`);

  const birthdays: Birthday[] = [];

  if (typeof flags.birthdayFile === 'undefined') {
    logger.error(
      `Can not parse birthdays if no source file is supplied. Use option 'birthdayFile'!`,
    );

    return birthdays;
  }

  const birthdayFile = join(
    cwd(),
    'resources',
    'birthday.csv',
    `${flags.birthdayFile}.csv`,
  );

  if (!existsSync(birthdayFile)) {
    logger.error(
      `File '${birthdayFile}' does not exist. Can not parse birthdays.`,
    );

    return [];
  }

  const buffer = await readFile(birthdayFile);
  const content = buffer.toString();

  content
    .split('\n')
    .forEach((line, lineIdx) => {
      // skip empty and comment lines
      if (typeof line !== 'string' || line === '' || line[0] === '#') {
        return;
      }

      const matches = line.match(
        /^(.*)\s?,\s?([0-9]{2})\.([0-9]{2})\.([0-9]{4})?$/,
      );

      if (!Array.isArray(matches) || matches.length < 4) {
        logger.warn(
          `Skipping line ${
            lineIdx + 1
          } because name/birthday could not be parsed.`,
        );

        return;
      }

      const name = matches[1].trim();
      const date = matches[2].trim();
      const month = matches[3].trim();
      let year;
      if (matches.length === 5) {
        year = matches[4];
      }

      const specialDayDate = new Date(firstDay);
      specialDayDate.setDate(parseInt(date));
      specialDayDate.setMonth(parseInt(month, 10) - 1);

      const specialDay = new Birthday(name, specialDayDate);

      if (typeof year !== 'undefined' && year !== '') {
        specialDay.sinceYear = parseInt(year, 10);
      } else {
        logger.warn(`Birthday "${line}" is missing a year!`);
      }

      birthdays.push(specialDay);
    });

  logger.info(`Parsed ${birthdays.length} birthdays.`);

  return birthdays;
};
