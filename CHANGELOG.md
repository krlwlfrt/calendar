## [0.0.2](https://gitlab.com/krlwlfrt/calendar/compare/v0.0.1...v0.0.2) (2024-11-29)



## [0.0.1](https://gitlab.com/krlwlfrt/calendar/compare/75dbc8e5ec69ea2f2e1da739441df9ceca8a73a5...v0.0.1) (2024-11-29)


### Bug Fixes

* add license information ([20e1872](https://gitlab.com/krlwlfrt/calendar/commit/20e187278e4255b28d1d6caa52bc1cb638192d51))
* add moving flag to all easter dates ([00535b1](https://gitlab.com/krlwlfrt/calendar/commit/00535b123ff199a1969ece6fbd226c116e6a1962))
* correctly determine dates for advents ([d5acbf6](https://gitlab.com/krlwlfrt/calendar/commit/d5acbf6f48c02ad24346d1e3f948b9d5c08cf8da))
* make birthday parser more robust with a regex ([3dab31d](https://gitlab.com/krlwlfrt/calendar/commit/3dab31def0683ef88dc4a8f100dc47b526dac212))
* output birthday files without .csv ([cb482c6](https://gitlab.com/krlwlfrt/calendar/commit/cb482c6ee36baab2d7b382807065be4a4ee3472a))
* skip empty cells in ferienwiki parser ([38db7ce](https://gitlab.com/krlwlfrt/calendar/commit/38db7ce4f6569664879cf74ab8b65c4eba73edc2))


### Features

* add backgrounds to images for months ([d817acf](https://gitlab.com/krlwlfrt/calendar/commit/d817acf890617bc937cbf2dd2cd37096fdea1e75))
* add example to generated output ([c467d6b](https://gitlab.com/krlwlfrt/calendar/commit/c467d6b5cd3db5a48bba6b12fb3311131b56eba8))
* add helper scripts for post processing ([bf8d96c](https://gitlab.com/krlwlfrt/calendar/commit/bf8d96cbc0a482c2c3b1bba8ba7bdcde358e393f))
* add initial implementation ([75dbc8e](https://gitlab.com/krlwlfrt/calendar/commit/75dbc8e5ec69ea2f2e1da739441df9ceca8a73a5))
* add new compiler html.perennial ([a90a8ec](https://gitlab.com/krlwlfrt/calendar/commit/a90a8ec3cea5e5cadeaa62e0736c3a45cb234ffe))
* add option for caching remote resources ([ec26425](https://gitlab.com/krlwlfrt/calendar/commit/ec26425c651a6cba75c54eba8960cedc852a58dd))
* add option to ignore cached files ([f13e79e](https://gitlab.com/krlwlfrt/calendar/commit/f13e79e9e503fd440cc255bd39269190c62b1551))
* add option to output only months ([f20ac8b](https://gitlab.com/krlwlfrt/calendar/commit/f20ac8b8f7f03bf54463c2fe551a7b7efa74f81d))
* add option to set a filename ([2401224](https://gitlab.com/krlwlfrt/calendar/commit/240122475cdeb9e88ed07bfc286b03b401ab01f8))
* add two column layout for list of birthdays ([27ee15f](https://gitlab.com/krlwlfrt/calendar/commit/27ee15f34c7bc22ef10bb0e6f1ceb3b314d9bc3d))
* make compatible with Deno, NodeJS and TS-Node ([3536a70](https://gitlab.com/krlwlfrt/calendar/commit/3536a7062e0d8e315a98aee1a59f950f3a4515f4))
* use higher dpi for conversion from pdf to jpeg ([8b4a9bb](https://gitlab.com/krlwlfrt/calendar/commit/8b4a9bb5fef62400f92fa5b6815aeee9a6db1c33))


### Reverts

* Revert "build: add missing npm dependency from jsr" ([3bf2b21](https://gitlab.com/krlwlfrt/calendar/commit/3bf2b21088728230e92e5dc3e804887741ae35cf))



