# @krlwlfrt/calendar

[TOC]

## Usage in Deno

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/krlwlfrt/calendar?branch=main&style=flat-square)](https://gitlab.com/krlwlfrt/calendar/commits/main)
[![jsr](https://img.shields.io/jsr/v/@krlwlfrt/calendar?style=flat-square)](https://jsr.io/@krlwlfrt/calendar)
[![license)](https://img.shields.io/gitlab/license/krlwlfrt%calendar?style=flat-square)](https://opensource.org/licenses/MIT)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://krlwlfrt.gitlab.io/calendar)

Command line interface to generate calendars.

### Permissions and their intents

| Flag            | Value                   | Intent                                             |
| --------------- | ----------------------- | -------------------------------------------------- |
| `--allow-env`   |                         | to access environment variables (see next section) |
| `--allow-net`   | `de.wikipedia.org:443`  | to get comemmorative days                          |
|                 | `www.ferienwiki.de:443` | to get holidays                                    |
| `--allow-read`  | `$PWD`                  | to read resources, compilers, parsers and plugins  |
|                 | `${which deno}`         | to get runtime path                                |
| `--allow-write` | `$PWD/write`            | to write the built calendar                        |

#### Patching

The dependency `npm:jsdom` transitively `npm:debug` which unfortunately
[blanket-accesses](https://github.com/debug-js/debug/issues/981) all environment
variables. To avoid having to use `--allow-env` for access to all environment
variables, you can apply the a patch to the module.

Just run the Deno task `patch`.

```sh
# applies node_modules-debug-src-node.js.patch to node_modules/debug/src/node.js
deno task patch
```

After patching you can use this much more reasonable list of environment
variables:

```sh
--allow-env=DEBUG,WS_NO_BUFFER_UTIL,FORCE_COLOR,CI,TEAMCITY_VERSION,TERM*,READABLE_STREAM,NODE_ENV_DIAGNOSTICS,COLORTERM
```

### Examples

#### Generate HTML calendar with default config

```sh
deno run jsr:@krlwlfrt/calendar html
```

To change the config, you can use the options of the command line interface.

#### See help and command line options

```sh
deno run jsr:@krlwlfrt/calendar --help
```

### Helpers

There are some helper scripts in the `helpers` directory which can aid you in
generating your calendar.

#### Compile and convert to images

The script `helpers/compile-and-convert-to-images.sh` compiles a calendar,
"prints" it to a PDF via headless Chromium and then generates one JPEG per page
of the PDF.

You can use the following list of environment variables to set values for the
corresponding command line interface options

| Variable     | Use                                                  |
| ------------ | ---------------------------------------------------- |
| YEAR         | year to generate calendar for                        |
| BIRTHDAYFILE | birthday file to use                                 |
| STATE        | state to get (school) holidays for                   |
| FILENAME     | filename for the calendar (*.html, *.pdf, ...)       |
| LANGUAGE     | language of the calendar                             |
| HTMLVARIANT  | HTML variant, see command line interface for options |
| PARSERS      | comma separated list of parsers                      |

#### HTML to PDF

The script `helpers/html-to-pdf.sh` "prints"/converts a HTML via headless
Chromium to a PDF.

#### PDF to JPEG

The script `helpers/pdf-to-jpeg.sh` generates one JPEG per page in a PDF.

## Usage in Node.js

```sh
npx jsr run jsr:@krlwlfrt/calendar --help
```
