#!/usr/bin/env bash

set -e
set -x

# set default values
NEXT_YEAR=`date +'%Y' -d 'next year'`
YEAR=${YEAR:-${NEXT_YEAR}}
BIRTHDAYFILE=${BIRTHDAYFILE:-example}
STATE=${STATE:-berlin}
FILENAME=${FILENAME:-index}
LANGUAGE=${LANGUAGE:-de}
HTMLVARIANT=${HTMLVARIANT:-classic}
PARSERS=${PARSERS:-wikipedia.de,easter,christmas,birthday.csv,holidays}
RUNTIME=${RUNTIME:-deno}
MODULE="mod.ts"
OPTIONS=""

if [ $RUNTIME = "node" ]; then
  MODULE="mod.js"
else
  RUNTIME="deno"
  OPTIONS="$OPTIONS \
  --allow-env=DEBUG,WS_NO_BUFFER_UTIL,FORCE_COLOR,CI,TEAMCITY_VERSION,TERM*,READABLE_STREAM,NODE_ENV,DIAGNOSTICS,COLORTERM \
  --allow-net=de.wikipedia.org:443,www.ferienwiki.de:443 \
  --allow-read="$PWD,$(which deno)" \
  --allow-write=$PWD"
fi

OPTIONS="$OPTIONS \
  $MODULE \
  compile \
  --language ${LANGUAGE} \
  --cacheRemoteSources \
  --birthdayFile $BIRTHDAYFILE \
  --htmlVariant $HTMLVARIANT \
  --state $STATE \
  --includeSchoolHolidays \
  --fileName $FILENAME \
  --year $YEAR \
  --parsers $PARSERS \
  html"

opts=( $OPTIONS )

if [ $RUNTIME = "node" ]; then
  node "${opts[@]}"
else
  deno run "${opts[@]}"
fi

# create dedicated build directory
mkdir -p build/$FILENAME-$YEAR/

# move generated calendar
mv "build/$FILENAME.html" "build/$FILENAME-$YEAR/"

# convert/print HTML to PDF
helpers/html-to-pdf.sh "build/$FILENAME-$YEAR/$FILENAME.html" "build/$FILENAME-$YEAR/$FILENAME.pdf"

# change into build directory
cd "build/$FILENAME-$YEAR"

# convert PDF to one JPEG per page
../../helpers/pdf-to-jpeg.sh "$FILENAME.pdf"
