#!/usr/bin/env bash

echo ""
echo "!!!!!     Printing/converting HTML to PDF. Can take a long time. Please be patient."
echo ""

if [ "$NO_SANDBOX" = 1 ]; then
  # needed for running as root in CI
  chromium \
    --headless \
    --disable-gpu \
    --virtual-time-budget=1000 \
    --print-to-pdf="$2" \
    --no-sandbox \
    "$1"
else
  chromium \
    --headless \
    --disable-gpu \
    --virtual-time-budget=1000 \
    --print-to-pdf="$2" \
    "$1"
fi
