// deno-lint-ignore-file no-explicit-any

import { assertEquals } from '@std/assert/equals';
import { describe, it } from '@std/testing/bdd';

export type Test = {
  args: any[];
  assertion: (...args: any[]) => any;
  expected?: any;
};

export type Suite = {
  fn: (...args: any[]) => any;
  tests: Test[];
};

/**
 * Run suites
 * @param suites Suites to run
 */
export function runSuites(suites: Suite[]) {
  for (const suite of suites) {
    const name = suite.fn.name.toString().replace(
      /([A-Z])/g,
      (match) => ` ${match.toLowerCase()}`,
    );
    describe(name, () => {
      for (const test of suite.tests) {
        it(`should ${name} for ${JSON.stringify(test.args)} right`, () => {
          test.assertion(suite.fn(...test.args), test.expected);
        });
      }
    });
  }
}

/**
 * Assert length
 * @typeParam T The type of the values to compare. This is usually inferred.
 * @param actual The actual value to compare.
 * @param expected The expected value to compare.
 * @param msg The optional message to display if the assertion fails.
 */
export function assertLength<T>(actual: T[], expected: number, msg?: string) {
  assertEquals(actual.length, expected, msg);
}
