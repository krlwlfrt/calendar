import { assert } from '@std/assert/assert';
import { assertEquals } from '@std/assert/equals';
import { assertFalse } from '@std/assert/false';
import {
  formatDate,
  getDayOfYear,
  getDaysInMonth,
  getDaysInYear,
  getSeason,
  getSpecificDaysInMonth,
  isLeap,
} from '../src/datetime.ts';
import { assertLength, runSuites } from './_test.ts';

runSuites([
  {
    fn: formatDate,
    tests: [
      {
        args: [new Date(2020, 0, 1), 'de', { month: 'long' }],
        assertion: assertEquals,
        expected: 'Januar',
      },
      {
        args: [new Date(2020, 11, 1), 'en', { month: 'long' }],
        assertion: assertEquals,
        expected: 'December',
      },
      {
        args: [new Date(2020, 2, 1), 'sv', { month: 'long' }],
        assertion: assertEquals,
        expected: 'mars',
      },
      {
        args: [new Date(2020, 0, 1), 'de', {
          day: '2-digit',
          month: '2-digit',
        }],
        assertion: assertEquals,
        expected: '01.01.',
      },
      {
        args: [new Date(2020, 1, 28), 'en', {
          day: '2-digit',
          month: '2-digit',
        }],
        assertion: assertEquals,
        expected: '02/28',
      },
      {
        args: [new Date(2020, 2, 1), 'sv', {
          day: '2-digit',
          month: '2-digit',
        }],
        assertion: assertEquals,
        expected: '01/03',
      },
    ],
  },
  {
    fn: getDaysInMonth,
    tests: [
      {
        args: [2019, 0],
        assertion: assertEquals,
        expected: 31,
      },
      {
        args: [2019, 1],
        assertion: assertEquals,
        expected: 28,
      },
      {
        args: [2020, 1],
        assertion: assertEquals,
        expected: 29,
      },
    ],
  },
  {
    fn: getDayOfYear,
    tests: [
      {
        args: [new Date(2024, 0, 1)],
        assertion: assertEquals,
        expected: 1,
      },
      {
        args: [new Date(2024, 1, 29)],
        assertion: assertEquals,
        expected: 60,
      },
      {
        args: [new Date(2024, 2, 8)],
        assertion: assertEquals,
        expected: 68,
      },
      {
        args: [new Date(2024, 5, 21)],
        assertion: assertEquals,
        expected: 173,
      },
      {
        args: [new Date(2024, 11, 31)],
        assertion: assertEquals,
        expected: 366,
      },
    ],
  },
  {
    fn: getDaysInYear,
    tests: [
      {
        args: [1900],
        assertion: assertEquals,
        expected: 365,
      },
      {
        args: [2000],
        assertion: assertEquals,
        expected: 366,
      },
      {
        args: [2004],
        assertion: assertEquals,
        expected: 366,
      },
      {
        args: [2007],
        assertion: assertEquals,
        expected: 365,
      },
    ],
  },
  {
    fn: getSeason,
    tests: [
      {
        args: [new Date(2000, 0, 1)],
        assertion: assertEquals,
        expected: 'winter',
      },
      {
        args: [new Date(2000, 2, 8)],
        assertion: assertEquals,
        expected: 'spring',
      },
      {
        args: [new Date(2000, 5, 21)],
        assertion: assertEquals,
        expected: 'summer',
      },
      {
        args: [new Date(2000, 8, 23)],
        assertion: assertEquals,
        expected: 'autumn',
      },
    ],
  },
  {
    fn: getSpecificDaysInMonth,
    tests: [
      {
        args: [2019, 0, 0],
        assertion: assertLength,
        expected: 4,
      },
      {
        args: [2019, 0, 1],
        assertion: assertLength,
        expected: 4,
      },
      {
        args: [2019, 0, 2],
        assertion: assertLength,
        expected: 5,
      },
      {
        args: [2019, 0, 3],
        assertion: assertLength,
        expected: 5,
      },
      {
        args: [2019, 0, 4],
        assertion: assertLength,
        expected: 5,
      },
      {
        args: [2019, 0, 5],
        assertion: assertLength,
        expected: 4,
      },
      {
        args: [2019, 0, 6],
        assertion: assertLength,
        expected: 4,
      },
    ],
  },
  {
    fn: isLeap,
    tests: [
      {
        args: [1900],
        assertion: assertFalse,
      },
      {
        args: [2000],
        assertion: assert,
      },
      {
        args: [2004],
        assertion: assert,
      },
      {
        args: [2007],
        assertion: assertFalse,
      },
    ],
  },
]);
