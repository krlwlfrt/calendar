import { assertEquals } from '@std/assert/equals';
import { describe, it } from '@std/testing/bdd';
import { defaultFlags } from '../../src/cli.ts';
import { parse } from '../../src/parsers/birthday.csv/index.ts';
import { Birthday } from '../../src/special-days.ts';

describe({
  name: 'birthday.csv parser',
  permissions: {
    read: [Deno.cwd()],
  },
  fn: () => {
    it('should parse birthdays', async () => {
      const birthdays = await parse(new Date(2024, 0, 1), {
        ...structuredClone(defaultFlags),
        birthdayFile: 'example',
      });

      assertEquals(birthdays, [
        new Birthday('Foo Bar', new Date(2024, 0, 1)),
        new Birthday('Bar Foo', new Date(2024, 1, 29), 2004),
        new Birthday('Lorem Ipsum', new Date(2024, 0, 1), 1969),
      ]);
    });
  },
});
