import { assertGreater } from '@std/assert/greater';
import { join } from '@std/path';
import { describe, it } from '@std/testing/bdd';
import { defaultFlags } from '../../src/cli.ts';
import { parse } from '../../src/parsers/holidays/index.ts';

describe({
  name: 'ferienwiki parser',
  permissions: {
    env: true,
    net: ['www.ferienwiki.de:443'],
    read: [Deno.cwd()],
    write: [join(Deno.cwd(), 'resources', 'ferienwiki')],
  },
  fn: () => {
    it('should parse school holidays', async () => {
      const schoolHolidays = await parse(new Date(2024, 0, 0), {
        ...structuredClone(defaultFlags),
        includeSchoolHolidays: true,
        state: 'brandenburg',
      });

      assertGreater(schoolHolidays.length, 0);
    });
  },
});
