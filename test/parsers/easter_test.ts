import { assertGreater } from '@std/assert/greater';
import { describe, it } from '@std/testing/bdd';
import { defaultFlags } from '../../src/cli.ts';
import { parse } from '../../src/parsers/easter/index.ts';

describe('easter parser', () => {
  it('should parse days related to easter', async () => {
    const specialDays = await parse(new Date(2024, 0, 1), {
      ...structuredClone(defaultFlags),
    });

    assertGreater(specialDays.length, 0);
  });
});
