import { assertGreater } from '@std/assert';
import { assertEquals } from '@std/assert/equals';
import { describe, it } from '@std/testing/bdd';
import { defaultFlags } from '../../src/cli.ts';
import { parse } from '../../src/parsers/christmas/index.ts';

describe('christmas parser', () => {
  it('should parse days related to christmas', async () => {
    const specialDays = await parse(new Date(2024, 0, 1), {
      ...structuredClone(defaultFlags),
    });

    assertEquals(specialDays.length, 6);

    assertEquals(
      specialDays.find((specialDay) => /Totensonntag/.test(specialDay.title))
        ?.date.getMonth(),
      10,
    );

    assertEquals(
      specialDays.find((specialDay) => /Volkstrauertag/.test(specialDay.title))
        ?.date.getMonth(),
      10,
    );

    assertGreater(
      specialDays.find((specialDay) => /Totensonntag/.test(specialDay.title))
        ?.date,
      specialDays.find((specialDay) => /Volkstrauertag/.test(specialDay.title))
        ?.date,
    );
  });
});
