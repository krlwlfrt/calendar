import { assertGreater } from '@std/assert/greater';
import { describe, it } from '@std/testing/bdd';
import { defaultFlags } from '../../src/cli.ts';
import { parse } from '../../src/parsers/wikipedia.de/index.ts';

describe({
  name: 'wikipedia.de parser',
  permissions: {
    env: true,
    net: ['de.wikipedia.org:443'],
    read: [Deno.cwd()],
    write: [Deno.cwd()],
  },
  fn: () => {
    it('should parse special days', async () => {
      const specialDays = await parse(new Date(2024, 0, 1), {
        ...structuredClone(defaultFlags),
      });

      assertGreater(specialDays.length, 0);
    });
  },
});
