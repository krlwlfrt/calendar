import { assertEquals } from '@std/assert/equals';
import { stripTags } from '../src/common.ts';
import { runSuites } from './_test.ts';

runSuites([
  {
    fn: stripTags,
    tests: [
      {
        args: ['<strong>Foo</strong><em>Bar</em>'],
        assertion: assertEquals,
        expected: 'FooBar',
      },
      {
        args: [`<!DOCTYPE html>
<html>
<body>

<h1>My First Heading</h1>

<p>My first paragraph.</p>

</body>
</html>`],
        assertion: assertEquals,
        expected: '\n\n\n\nMy First Heading\n\nMy first paragraph.\n\n\n',
      },
    ],
  },
]);
