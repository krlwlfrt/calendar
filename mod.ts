/**
 * [![pipeline status](https://img.shields.io/gitlab/pipeline-status/krlwlfrt%calendar?branch=main&style=flat-square)](https://gitlab.com/krlwlfrt/calendar/commits/main)
 * [![jsr](https://img.shields.io/jsr/v/@krlwlfrt/calendar?style=flat-square)](https://jsr.io/@krlwlfrt/calendar)
 * [![license)](https://img.shields.io/gitlab/license/krlwlfrt%calendar?style=flat-square)](https://opensource.org/licenses/MIT)
 * [![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://krlwlfrt.gitlab.io/calendar)
 *
 * Command line interface to generate calendars.
 *
 * ## Permissions and their intents
 *
 * | Flag            | Value                   | Intent                                             |
 * | --------------- | ----------------------- | -------------------------------------------------- |
 * | `--allow-env`   |                         | to access environment variables (see next section) |
 * | `--allow-net`   | `de.wikipedia.org:443`  | to get comemmorative days                          |
 * |                 | `www.ferienwiki.de:443` | to get holidays                                    |
 * | `--allow-read`  | `$PWD`                  | to read resources, compilers, parsers and plugins  |
 * |                 | `${which deno}`         | to get runtime path                                |
 * | `--allow-write` | `$PWD/write`            | to write the built calendar                        |
 *
 * ### Patching
 *
 * The dependency `npm:jsdom` uses `npm:debug` which unfortunately blanket-accesses
 * all environment variables. To avoid having to use `--allow-env` for access to
 * all environment variables, you can apply the a patch to the module.
 *
 * Just run the Deno task `patch`.
 *
 * ```sh
 * # applies node_modules-debug-src-node.js.patch to node_modules/debug/src/node.js
 * deno task patch
 * ```
 *
 * After patching you can use this much more reasonable list of environment
 * variables:
 *
 * ```sh
 * --allow-env=DEBUG,WS_NO_BUFFER_UTIL,FORCE_COLOR,CI,TEAMCITY_VERSION,TERM*,READABLE_STREAM,NODE_ENV_DIAGNOSTICS,COLORTERM
 * ```
 *
 * ## Examples
 *
 * ### Generate HTML calendar with default config
 *
 * ```sh
 * deno run jsr:@krlwlfrt/calendar html
 * ```
 *
 * To change the config, you can use the options of the command line interface.
 *
 * ### See help and command line options
 *
 * ```sh
 * deno run jsr:@krlwlfrt/calendar --help
 * ```
 *
 * @module
 */

import { type Hooks, loadModules } from '@krlwlfrt/dml';
import { existsSync } from 'node:fs';
import { join } from 'node:path';
import { cwd } from 'node:process';
import { commandLineInterface } from './src/cli.ts';
import {
  type CompilerModule,
  compilerModules,
  type ParserModule,
  parserModules,
} from './src/modules.ts';

const compilerPluginsPath = join(cwd(), 'plugins', 'compilers');
const parserPluginsPath = join(cwd(), 'plugins', 'parsers');

const hooks: Hooks = {
  beforeLoad: (_pathToLoad) => {
    // reset the command line interface to ensure that global options are actually global
    commandLineInterface.reset();

    // load all modules
    return Promise.resolve(true);
  },
};

Promise.all([
  // load compiler modules
  loadModules(join(cwd(), 'src', 'compilers'), true, hooks),

  // load plugin compiler modules
  existsSync(compilerPluginsPath)
    ? loadModules(compilerPluginsPath, true, hooks)
    : [],

  // load parser modules
  loadModules(join(cwd(), 'src', 'parsers'), true, hooks),

  // load plugin parser modules
  existsSync(parserPluginsPath)
    ? loadModules(parserPluginsPath, true, hooks)
    : [],
]).then((loadedModules) => {
  // add loaded compiler modules
  compilerModules.push(
    ...loadedModules[0] as CompilerModule[],
    ...loadedModules[1] as CompilerModule[],
  );

  // add loaded parser modules
  parserModules.push(
    ...loadedModules[2] as ParserModule[],
    ...loadedModules[3] as ParserModule[],
  );

  commandLineInterface.parse();
});
