import { loadModules } from '@krlwlfrt/dml';
import { exists } from '@std/fs';
import { join } from '@std/path';
import { denoPlugins } from 'jsr:@luca/esbuild-deno-loader';
import { cwd } from 'node:process';
import * as esbuild from 'npm:esbuild';

/**
 * Get relevant files from directy
 * @param directory Directory got get relevant files from
 * @returns Array of relevant files
 */
async function getFiles(directory: string) {
  if (!(await exists(directory))) {
    console.log(`Directory '${directory}' does not exist. Skipping...`);
    return [];
  }

  const modules = await loadModules(directory);

  const files = modules.map((module) => module.path);

  return files;
}

const plugins = [
  ...await getFiles(join(Deno.cwd(), 'plugins', 'compilers')),
  ...await getFiles(join(Deno.cwd(), 'plugins', 'parsers')),
];

await esbuild.build({
  plugins: [...denoPlugins()],
  entryPoints: [
    './mod.ts',
    ...await getFiles(join(cwd(), 'src', 'compilers')),
    ...await getFiles(join(cwd(), 'src', 'parsers')),
    ...plugins,
  ],
  outdir: './',
  bundle: true,
  format: 'esm',
  logLevel: 'info',
  external: [
    'node:*',
    '@krlwlfrt/dml',
    'commander',
    'date-easter',
    'jsdom',
    'mustache',
    'paper-css',
    'winston',
  ],
  minify: true,
  treeShaking: true,
  target: 'node22',
  splitting: true,
  chunkNames: './src/chunks/[name]-[hash]',
});

esbuild.stop();
